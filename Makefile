CXX = g++

ifeq ($(DEBUG),1)
	CXXFLAGS = -fdiagnostics-color=always -g -O0 -Wall -Wextra -Wunreachable-code -Werror -std=c++11
else
	CXXFLAGS = -O3 -Wall -Wextra -Wunreachable-code -Werror -std=c++11
endif
# -ansi -pedantic -Wwrite-strings -Wstrict-prototypes
 
CXXTEST = cxxtestgen
CXXTESTFLAGS = --error-printer

LIBS = -larmadillo

# sources
SRCDIR = src
SRCS = $(filter-out main.cpp, $(notdir $(wildcard $(SRCDIR)/*.cpp)))
#$(warning $(SRCS))

# tests sources
TESTDIR = tests
TESTS = $(wildcard $(TESTDIR)/*.h)

# target
TARGETDIR = bin
TARGET = $(TARGETDIR)/main

# tests targets
TESTTARGET = $(TARGETDIR)/tests

# obj
OBJDIR = obj
OBJS = $(addprefix $(OBJDIR)/, $(SRCS:cpp=o))
#$(warning $(OBJS))
MAINOBJ = $(OBJDIR)/main.o

# tests obj
TESTSRC = $(OBJDIR)/tests.cpp
TESTOBJ = $(OBJDIR)/tests.o


# lib
LIBDIR = lib
LIB = $(LIBDIR)/mylib.a

# doc
DOCDIR = Doxygen

.PHONY: all
all:
	$(MAKE) astyle
	$(MAKE) mkdirectory
	$(MAKE) $(LIB)
	$(MAKE) $(TARGET)
	$(MAKE) $(TESTTARGET)
	$(MAKE) runtests
	$(MAKE) doc

$(TARGET): $(MAINOBJ) $(LIB)
	$(CXX) -o $(TARGET) $(MAINOBJ) $(LIB) $(CXXFLAGS) $(LIBS)

$(TESTTARGET): $(LIB)
	$(CXXTEST) -o $(TESTSRC) $(CXXTESTFLAGS) $(TESTS)
	$(CXX) -o $(TESTOBJ) -c $(TESTSRC) $(CXXFLAGS)
	$(CXX) -o $(TESTTARGET) $(TESTOBJ) $(LIB) $(CXXFLAGS) $(LIBS)

$(LIB): $(OBJS)
	ar crv $(LIB) $(OBJS)

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	$(CXX) -o $@ -c $< $(CXXFLAGS)


.PHONY: astyle
astyle:
	astyle --options=astyle.conf "./src/*.cpp" "./include/*.h" | grep -v "Unchanged"
	find ./src/ -name "*.cpp.orig" -exec rm -v {} \;
	find ./include/ -name "*.h.orig" -exec rm -v {} \;

.PHONY: doc
doc:
	doxygen

.PHONY: runtests
runtests:
	./bin/tests

.PHONY: run
run: $(TARGET)
	./bin/main

.PHONY: mkdirectory
mkdirectory:
	mkdir -p $(TARGETDIR)
	mkdir -p $(OBJDIR)
	mkdir -p $(LIBDIR)
	mkdir -p $(DOCDIR)

.PHONY: clean
clean:
	rm -rf $(TARGETDIR)
	rm -rf $(OBJDIR)
	rm -rf $(LIBDIR)
	rm -rf $(DOCDIR)

.PHONY: render #render the scene
render: run
	(cd plot ;  povray +A0.0001 -W800 -H600 +P +Q11 scene_.pov -d)
