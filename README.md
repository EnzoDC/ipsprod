# Résolution des équations d'un oscillateur harmonique quantique 1D

Instructions de build :

- Installer des versions récentes de **doxygen**, **astyle**, **cxxtest**, **armadillo**, **graphviz**, **g++**
- Lancer la commande `make`
- La doc est générée dans **Doxygen/**, l'éxécutable principal dans **bin/main**, et les tests dans **bin/tests**

TODO : 

- [x] calculate Hermite and Generalized Laguerre polynomials

- [x] calculate z- and r- functions

- [x] define the basis quantum number values

- [~] calculate the nuclear local density with the naive algorithm

- [ ] calculate the nuclear local density with an optimized algorithm

- [x] implement the mandatory unit tests

- [ ] plot the nuclear local density in the (x,z)(x,z) plane (with Matplotlib, Plotly.js...)

- [ ] plot the nuclear local density in the (x,y,z)(x,y,z) space (with POV-Ray, Blender...)

- [ ] present your results using the presentation template
