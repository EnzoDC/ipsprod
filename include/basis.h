#ifndef BASIS_H
#define BASIS_H

#include <armadillo>

/**
 * @brief Basis class to compute the correct quantum numbers following the given basis truncation
 *
 */
class Basis {
private:
	double v(const int, const int, const double);
public:
	Basis(const double, const double, const int, const double);

	double br; /*!< radial deformation parameter*/
	double bz;/*!< axial deformation parameter*/

	int mMax; /*!< maximum first quantum number*/
	arma::ivec nMax; /*!< Array of size mMax containing the maximum second quantum number for each m up to mMax. This number is decreasing*/
	arma::imat n_zMax; /*!< Matrix of size mMax * nMax(0) containing the maximum third quantum number for each pair of n, m**/

	arma::vec rPart(const arma::vec&, const int, const int);
	arma::vec zPart(const arma::vec&, const int);
};

#endif /*INCLUDE_BASIS_H*/
