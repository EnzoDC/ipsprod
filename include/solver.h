#ifndef INCLUDE_SOLVER_H
#define INCLUDE_SOLVER_H

#include "../include/basis.h"

/**
 * @brief This class contains the algorithms needed to compute the nuclear local density
 *
 */
class Solver {

private:

public:
	const arma::mat solve_naif(Basis &basis, const arma::vec &zVals, const arma::vec &rVals, const arma::mat &rho);
	const arma::mat solve(Basis &basis, const arma::vec &zVals, const arma::vec &rVals, const arma::mat &rho);
	const arma::cube rotate(const arma::mat &data2D, const arma::vec &rVals);
};

#endif /* INCLUDE_SOLVER_H */
