#ifndef CONST_H
#define CONST_H

#include <armadillo>
/**
 * @file const.h
 *
 * @brief this file defines various constant that will be used throughout other files.
 */

/**
 * @var extern const double br
 * @brief radial deformation parameter
 */
extern const double br;

/**
 * @var extern const double bz
 * @brief axial deformation parameter
 */
extern const double bz;

/**
 * @var extern const double N
 * @brief First basis truncation parameter
 */
extern const double N;

/**
 * @var extern const double Q
 * @brief Second basis truncation parameter
 */
extern const double Q;

/**
 * @var extern const arma::rowvec z
 * @brief Positions to evaluate. (unit : fm)
 */
extern const arma::vec z;

/**
 * @var extern const arma::rowvec r
 * @brief Positions to evaluate. (unit : fm)
 */
extern const arma::vec r;

#endif //CONST_H
