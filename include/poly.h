#ifndef POLY_H
#define POLY_H

#include <armadillo>
/**
 * @class Poly
 *
 * @brief This class can compute and store Hermite and generalized Laguerre polynomials
 *
 */
class Poly {
public:
	arma::mat H; /*!< This matrix stores the Hermite polynomials (1per row)*/
	arma::cube L; /*!< This cube stores the Laguerre polynomials (n defines a slice, m defines a row)*/
	Poly(); /*!< Default constructor*/
	void calcHermite(const int, const arma::vec&);/*!< Computes Hermite polynomials for given points for indexes 0 to n - 1*/
	arma::vec hermite(const int); /*!< Retrieve a Hermite polynomial already computed (throws an error if index is out of bounds)*/
	void calcLaguerre(const int, const int, const arma::vec&); /*!< Computes Laguerre polynomials for given points for indexes 0 to n - 1 and 0 to m - 1*/
	arma::vec laguerre(const int, const int);/*!< Retrieve a Laguerre polynomial already computed (throws an error if index is out of bounds)*/
};

#endif
