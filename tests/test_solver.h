#include <cxxtest/TestSuite.h>
#include <armadillo>

#include "../include/solver.h"
#include "../include/const.h"

class TestSolver : public CxxTest::TestSuite {

public:

	void testConsistency(){
		// Test if the optimized algorithm gives results consistent with the naive algorithm
		arma::mat rho;
		rho.load("./data/rho.arma", arma::arma_ascii);
		Basis b = Basis(br, bz, N, Q);

		Solver s = Solver();
		arma::mat res = s.solve_naif(b, z, r, rho);
		arma::mat res_opti = s.solve(b, z, r, rho);
		TS_ASSERT_DELTA(arma::norm(res - res_opti), 0.0, 1e-14);
	}
	
};