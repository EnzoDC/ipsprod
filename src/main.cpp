#include <iostream>
#include <fstream>
#include <armadillo>
#include <chrono>
#include "../include/const.h"
#include "../include/basis.h"
#include "../include/solver.h"

//code written by our professor
std::string cubeToDf3(const arma::cube &m) {
	std::stringstream ss(std::stringstream::out | std::stringstream::binary);
	int nx = m.n_rows;
	int ny = m.n_cols;
	int nz = m.n_slices;
	ss.put(nx >> 8);
	ss.put(nx & 0xff);
	ss.put(ny >> 8);
	ss.put(ny & 0xff);
	ss.put(nz >> 8);
	ss.put(nz & 0xff);
	double theMin = 0.0;
	double theMax = m.max();
	for (uint k = 0; k < m.n_slices; k++) {
		for (uint j = 0; j < m.n_cols; j++) {
			for (uint i = 0; i < m.n_rows; i++) {
				uint v = 255 * (fabs(m(i, j, k)) - theMin) / (theMax - theMin);
				ss.put(v);
			}
		}
	}
	return ss.str();
}

int main() {
	arma::mat rho;
	rho.load("./data/rho.arma", arma::arma_ascii);

	Basis b = Basis(br, bz, N, Q);

	Solver s = Solver();
	std::chrono::steady_clock::time_point start;
	std::chrono::steady_clock::time_point end;

	start = std::chrono::steady_clock::now();
	arma::mat res = s.solve_naif(b, z, r, rho);
	end = std::chrono::steady_clock::now();
	std::cout << "solve naif: " << double(std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()) / 1000.0 << "[ms]" << std::endl;

	start = std::chrono::steady_clock::now();
	arma::mat res_opti = s.solve(b, z, r, rho);
	end = std::chrono::steady_clock::now();
	std::cout << "solve: " << double(std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()) / 1000.0 << "[ms]" << std::endl;

	double norm = arma::norm(res - res_opti);
	std::cout << "norm: " << norm << std::endl;
	res.save("./data/result.csv", arma::csv_ascii);
	res_opti.save("./data/result_opti.csv", arma::csv_ascii);

	arma::cube data3D = s.rotate(res_opti, r);
	//writing to file :
	std::ofstream data3Dfile("./data/data3D.df3");
	data3Dfile << cubeToDf3(data3D);
	data3Dfile.close();
}
