#include <armadillo>
#include <chrono>

#include "../include/solver.h"

/**
 * @brief Computes the local nuclear density
 *
 * @param basis the basis to compute with (must correspond with rho)
 * @param zVals the z values to compute
 * @param rVals the r values to compute
 * @param rho the density matrix
 * @return arma::mat The nuclear local density matrix, with z on each line, and r on each column
 */
const arma::mat Solver::solve_naif(Basis &basis, const arma::vec &zVals, const arma::vec &rVals, const arma::mat &rho) {
	arma::mat result = arma::mat(zVals.n_elem, rVals.n_elem, arma::fill::zeros); // number of points on r- and z- axes
	int x = 0;
	for (int m = 0; m < basis.mMax; m++) {
		for (int n = 0; n < basis.nMax(m); n++) {
			for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++) {
				int y = 0;
				for (int mp = 0; mp < basis.mMax; mp++) {
					for (int np = 0; np < basis.nMax(mp); np++) {
						for (int n_zp = 0; n_zp < basis.n_zMax(mp, np); n_zp++) {
							arma::vec Z = basis.zPart(zVals, n_z); //basisFunc(m, n, n_z, zVals, rVals);
							arma::rowvec R = basis.rPart(rVals, m, n).t(); //basisFunc(mp, np, n_zp, zVals, rVals);
							arma::mat ZR = Z * R;
							arma::vec Zp = basis.zPart(zVals, n_zp); //basisFunc(m, n, n_z, zVals, rVals);
							arma::rowvec Rp = basis.rPart(rVals, mp, np).t(); //basisFunc(mp, np, n_zp, zVals, rVals);
							arma::mat ZRp = Zp * Rp;
							result += (ZR % ZRp) * rho(x, y); // mat += mat % mat * double
							y++;
						}
					}
				}
				x++;
			}
		}
	}

	return result;
}


/**
 * @brief Computes the local nuclear density, using an optimized algorithm
 *
 * @param basis the basis to compute with (must correspond with rho)
 * @param zVals the z values to compute
 * @param rVals the r values to compute
 * @param rho the density matrix
 * @return arma::mat The nuclear local density matrix, with z on each line, and r on each column
 */
const arma::mat Solver::solve(Basis &basis, const arma::vec &zVals, const arma::vec &rVals, const arma::mat &rho) {


//	std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

	arma::cube ZRs = arma::cube(zVals.n_elem, rVals.n_elem, rho.n_cols);
	int i = 0;
	for (int m = 0; m < basis.mMax; m++) {
		for (int n = 0; n < basis.nMax(m); n++) {
			arma::rowvec rPart = basis.rPart(rVals, m, n).t();
			for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++) {
				ZRs.slice(i) = basis.zPart(zVals, n_z) * rPart;
				++i;
			}
		}
	}

//	std::chrono::steady_clock::time_point middle = std::chrono::steady_clock::now();

	arma::mat result = arma::mat(zVals.n_elem, rVals.n_elem, arma::fill::zeros); // number of points on r- and z- axes
	int mBlock = 0;
	int x = 0;
	for (int m = 0; m < basis.mMax; m++) {
		for (int n = 0; n < basis.nMax(m); n++) {
			for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++) {
				arma::mat ZR = ZRs.slice(x);
				for(int y = mBlock; y < x; ++y) {
					result += (ZR % ZRs.slice(y)) * (rho(x, y));
				}
				result += (ZR % ZR) * rho(x, x) / 2;
				++x;
			}
		}
		mBlock = x;
	}
	result *= 2;

//	std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

//	std::cout << "solve Part1: " << double(std::chrono::duration_cast<std::chrono::microseconds>(middle - begin).count()) / 1000.0 << "[ms]" << std::endl;
//	std::cout << "solve Part2: " << double(std::chrono::duration_cast<std::chrono::microseconds>(end - middle).count()) / 1000.0 << "[ms]" << std::endl;
//	std::cout << "solve Result: " << double(std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count()) / 1000.0 << "[ms]" << std::endl;
	return result;
}


/**
 * @brief Returns the cube obtained by rotating a matrix along the z-axis
 *
 * @param data2D the data to rotate
 * @param rVals the points along the r-axis on the data. This is assumed to be sorted in ascending order
 * @return arma::cube the rotated matrix
 */
const arma::cube Solver::rotate(const arma::mat &data2D, const arma::vec &rVals) {
	arma::cube result = arma::cube(2 * rVals.n_elem, 2 * rVals.n_elem, data2D.n_rows);
	arma::vec completeRVals = arma::vec(2 * rVals.n_elem);
	//fill completeRVals with all possible values for the x and y axis
	completeRVals.subvec(0, rVals.n_elem - 1) = arma::reverse(-rVals);
	completeRVals.subvec(rVals.n_elem, 2*rVals.n_elem - 1) = rVals;
	//Square each value
	completeRVals %= completeRVals;
	//Create an x value meshgrid
	arma::mat xMat = arma::mat(2 * rVals.n_elem, 2 * rVals.n_elem);
	xMat.each_col([&completeRVals](arma::vec &col) {
		col = completeRVals;
	});
	//rGrid contains the radius corresponding to each x and y value
	arma::mat rGrid = arma::pow(xMat + xMat.t(), 0.5) * rVals.n_elem / rVals(rVals.n_elem - 1);
	//Decompose rGrid
	arma::imat lowerRGrid = arma::conv_to<arma::imat>::from(rGrid);
	arma::mat ratio = rGrid - lowerRGrid;

	for (unsigned int z = 0; z < result.n_slices; z++) {
		for (unsigned int x = 0; x < result.n_cols; x++) {
			for (unsigned int y = 0; y < result.n_rows; y++) {
				if (rGrid(y, x) >= rVals.n_elem - 1) {
					result(y, x, z) = 0;
				} else {
					result(y, x, z) = data2D(z, lowerRGrid(y, x)) * ratio(y, x) + data2D(z, 1 + lowerRGrid(y, x)) * (1 - ratio(y, x));
				}
			}
		}

	}
	return result;
}

