#include "../include/const.h"
#include <math.h>


const double br = 1.935801664793151;

const double bz = 2.829683956491218;

const double N = 14;

const double Q = 1.3;

const arma::vec r = arma::linspace(-10, 10, 32).subvec(16, 31);

const arma::vec z = arma::linspace(-20, 20, 64);

