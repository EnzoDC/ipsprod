#include "../include/basis.h"
#include "../include/poly.h"

#include <math.h>

/**
 * @brief Constructs a basis with given parameters
 *
 * @param _br the radial deformation parameter
 * @param _bz the axial deformation parameters
 * @param N > 0 first basis truncation parameter
 * @param Q >= 0 second basis truncation parameter
 */
Basis::Basis(const double _br, const double _bz, const int N, const double Q) {

	br = _br;
	bz = _bz;
	//find the mMax
	int i = 1;
	while (v(i, N, Q) >= 1) {
		i++;
	}
	mMax = i - 1; //valid m are 0<=m<mMax

	nMax = arma::regspace<arma::ivec>(0, 1, mMax - 1); //every nMax is first instancied with all possible ms
	nMax = (-nMax + mMax - 1) / 2 + 1; //every possible n are between 0<=n<nMax[m]

	//making n_zMax
	n_zMax = arma::imat(mMax, nMax(0), arma::fill::zeros);
	for (int m = 0; m < mMax; m++) {
		for (int n = 0; n < nMax(m); n++) {
			n_zMax(m, n) = v(m + 2 * n + 1, N, Q);
		}
	}
}

/**
 * @brief basis truncation function
 *
 * @param i the index to evaluate the function at
 * @param N first basis truncation parameter
 * @param Q second basis truncation parameter
 */
double Basis::v(const int i, const int N, const double Q) {
	return (N + 2) * pow(Q, 2. / 3.) + 0.5 - (i * Q);
}

/**
 * @brief Computes the radial part of the basis on given points, with given quantum numbers
 *
 * @param r the points to evaluate
 * @param m first quantum number
 * @param n second quantum number
 */
arma::vec Basis::rPart(const arma::vec &r, const int m, const int n) {
	int prod;
	if (m == 0) {
		prod = 1;
	} else {
		prod = n + 1;
		for (int i = n + 2; i <= n + m; i++) {
			prod *= i;
		}
	}
	Poly poly;
	poly.calcLaguerre(std::max(m+1, 2), std::max(n+1, 2), arma::pow(r/br, 2));
	double factor = 1 / (br * sqrt(M_PI * prod));
	arma::vec exponential = arma::exp(- arma::pow(r/br, 2) / 2);
	arma::vec power = arma::pow(r / br, m);
	return factor * (exponential % power % poly.laguerre(m, n));
}

/**
 * @brief Computes the axial part of the basis on given points, with given quantum number
 *
 * @param z the points to evaluate
 * @param nz quantum number
 */
arma::vec Basis::zPart(const arma::vec &z, const int nz) {
	Poly poly;
	poly.calcHermite(std::max(nz+1, 2), z / bz);
	// Calculation of 1/sqrt(2^nz * nz! * bz * sqrt(pi))
	long unsigned int factorial = 1; // Tests don't pass if this is an int or unsigned int
	for (int i = 1; i <= nz; i++) {
		factorial *= 2 * i;
	}
	double factor = 1 / sqrt(sqrt(M_PI) * bz * factorial);

	// Calculation of e^(-z²/2bz²)
	arma::vec exponentials = arma::exp(- arma::pow(z/bz, 2) / 2);
	// Calculation of zPart
	return factor * (exponentials % poly.hermite(nz));
}
