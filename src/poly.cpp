#include <armadillo>

#include "../include/poly.h"

Poly::Poly() {

}

/**
 * @brief Evaluates the Hermite polynomial for all indices until n, on given points, and store the results internally
 *
 * @param n the (exclusive) maximum index to evaluate
 * @param z the points to evaluate
 */
void Poly::calcHermite(const int n, const arma::vec &z) {
	H = arma::mat(z.n_elem, n);
	H.col(0) = arma::vec(z.n_elem, arma::fill::ones);
	H.col(1) = z * 2;
	for (unsigned int i = 2; i < H.n_cols; i++) {
		H.col(i) = 2 * (z % H.col(i - 1)) - 2 * (i - 1) * H.col(i - 2);
	}
}

/**
 * @brief Retrieves computed Hermite values (DOES NOT CHECK INDEX BOUNDS)
 *
 * @param n the polynomial to retrieve
 * @return arma::vec the pre-computed values
 */
arma::vec Poly::hermite(const int n) {
	return H.col(n);
}

/**
 * @brief Evaluates the Laguerre polynomial for all indices until (m,n), on given points, and stores the results internally
 *
 * @param m the (exclusive) maximum first index
 * @param n the (exclusive) maximum second index
 * @param z the points to evaluate
 */
void Poly::calcLaguerre(const int m, const int n, const arma::vec &z) {
	L = arma::cube(z.n_elem, m, n);
	arma::mat M = arma::vec(z.n_elem, arma::fill::ones) * arma::regspace<arma::rowvec>(0, 1, m - 1);
	arma::mat Z = z * arma::rowvec(m, arma::fill::ones);
	L.slice(0) = arma::mat(z.n_elem, m, arma::fill::ones); // L(m, 0, z) = 1
	L.slice(1) = 1 + M - Z; // L(m, 1, z) = 1 + m - z
	for (unsigned int i = 2; i < L.n_slices; i++) {
		// L(m, n, z) = (2 + (m - 1 - z) / n) * L(m, n-1, z) - (1 + (m - 1) / n) * L(m, n-2, z)
		L.slice(i) = (2 + (M - 1 - Z) / i) % L.slice(i - 1) - (1 + (M - 1) / i) % L.slice(i - 2);
	}
}

/**
 * @brief Retrieves computed Laguerre values (DOES NOT CHECK INDEX BOUNDS)
 *
 * @param m first index
 * @param n second index
 * @return arma::vec the pre-computed values
 */
arma::vec Poly::laguerre(const int m, const int n) {
	return L.slice(n).col(m);
}
